<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'IndexController@index')->name('index');
Route::get('/fund_account', 'FundaccountController@index')->name('fund-account');
Route::get('/payout', 'PayoutController@index')->name('payout')->middleware('verified');
Route::post('/payout/save', 'PayoutController@savePayout')->name('save-payout');

// Routes for games
Route::get('/games/words', 'HomeController@wordGames')->name('word-games');
Route::get('/games/quotes', 'HomeController@quoteGames')->name('quote-games');
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');


// Route for admins
Route::get('/admin/winners-board', 'AdminController@winnersBoard')->name('winners-board');
Route::get('/admin/games', 'Admincontroller@games')->name('games');
Route::get('/admin/winners', 'AdminController@winners')->name('winners');
Route::get('/admin/make-winner/{id}', 'AdminController@makeWinner')->name('make-winner');
Route::get('/admin', 'AdminController@admin')->name('admin-home')->middleware('admin');
Route::get('/admin/add-word', 'AdminController@addWord')->name('add-word');
Route::post('/admin/save-word', 'AdminController@saveWord')->name('save-word');
Route::get('/admin/payouts', 'AdminController@payouts')->name('payouts');
Route::get('/admin/game-details/{id}', 'AdminController@gameDetails')->name('game-details');
Route::get('/admin/players', 'AdminController@players')->name('players');
Route::put('/admin/make-admins/{id}', 'AdminController@makeAdmin')->name('make-admin');
Route::put('/admin/make-player/{id}', 'AdminController@makePlayer')->name('make-player');
Route::get('/admin/admins', 'AdminController@admins')->name('admins');
Route::put('/admin/edit-playbalance/{id}', 'AdminController@editPlayBalance')->name('editPlay-balance');
Route::delete('/admin/delete-player/{id}', 'AdminController@deletePlayer')->name('delete-player');
Route::delete('/admin/delete-admin/{id}','AdminController@deleteAdmin')->name('delete-admin');
Route::put('/admin/update-payout/{id}', 'adminController@updatePayout')->name('update-payout');
Route::get('/admin/pending-payouts', 'AdminController@pendingPayout')->name('pending-payouts');
Route::put('/admin/start-game/{id}', 'AdminController@startGame')->name('start-game');;
Route::put('/admin/end-game/{id}', 'AdminController@endGame')->name('end-game');
Route::put('/admin/end-all', 'AdminController@endAllGames')->name('end-all');
Route::put('/admin/start-all', 'AdminController@startAllGames')->name('start-all');
Route::get('/admin/find-users', 'AdminController@findUsers')->name('find-users');


Auth::routes(['verify' => true]);

// These handles the game play and the result for the users and players
Route::post('/game-play/{id}', 'GamePlayController@gamePlay')->name('game-play')->middleware('verified');
Route::get('/game/winners/{id}', 'GamePlayController@gameWinners')->name('winners');

// Static pages
Route::get('/contact-us', 'StaticController@contactUs')->name('contact-us');
Route::get('/howtoplay', 'StaticController@howToPlay')->name('howtoplay');
Route::get('/help', 'StaticController@help')->name('help');
Route::get('/terms-and-conditions', 'StaticController@termsAndConditions')->name('terms-and-conditions');


Route::get('/user/transactions/{id}', 'TransactionsController@userTrans')->name('user-trans');
Route::get('/user/history/{id}', 'HistoryController@playHistory')->name('history');
Route::get('/user/history/{id}/details', 'HistoryController@historyDetails')->name('history-details');
