@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-7" style="margin:auto;">
    <p style="padding-top:30px;"><a class="btn btn-primary" id="wordBtn" href="{{ route('word-games')}}">Words</a> <a id="quoteBtn" class="btn btn-success" href="{{ route('quote-games')}}">Quotes</a> <a style="display:none;" class="btn btn-danger" href="">Quest</a></p>
    @include('inc.flash-messages')
    <h4> Quote Games</h4>
    @if(count($quoteGames)> 0)
    @foreach($quoteGames as $g)
          <div class="card games">
            <table>
              <thead>
                <tr>
                  <th>Instruction</th>
                  <th>Quote to find</th>
                  <th>Game Point</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="width:300px;">{{$g->instruction}}</td>
                  <td style="width:250px;"><strong>{{$g->enscripted_word}}</strong></td>
                  <td style="width:250px;">{{$g->final_point}}</td>
                </tr>
              </tbody>
            </table>
            <hr>
            <form action="{{ route('game-play', $g->id)}}" method="POST" >
            @csrf
                <div class="input-group">
                    <input type="text" name="word" class="form-control" placeholder="Enter the correct word {{$g->id}}">
                    <input type="text" name="stake" class="form-control" placeholder="0.0">
                    <input type="submit" value="Play" class="btn btn-default" style="
                     box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
                    -moz-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
                    -webkit-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
                    -o-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
                    -ms-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
                    background-image: linear-gradient(to right, #0f1b27  0%, rgb(251, 100, 5) 100%), radial-gradient(circle at top left, #0f1b27, rgb(251, 100, 5));
                    background-image: -webkit-linear-gradient(to right, #070323 0%, #d0418e 100%), -webkit-radial-gradient(circle at top left, #8252fa, #eca2f1);
                    background-image: -moz-linear-gradient(to right, #070323 0%, #d0418e 100%), -moz-radial-gradient(circle at top left, #8252fa, #eca2f1);
                    background-image: -ms-linear-gradient(to right, #070323 0%, #d0418e 100%), -ms-radial-gradient(circle at top left, #8252fa, #eca2f1);
                    background-image: -o-linear-gradient(to right, #070323 0%, #d0418e 100%), -o-radial-gradient(circle at top left, #8252fa, #eca2f1);
                    border:none; color: #fff;
                    ">
                </div>
            </form>
          </div>


          @endforeach
          @else 
          <p>No Quote  avialable as these moment!!</p>
          @endif
       </div>
         
  </div>
</div>

@include('inc.footer')
@endsection

<style>
  th {
    padding-left: 10px; 
    padding-right: 10px;
    padding-top:10px;
    padding-bottom: 10px;
  }
  td {
    padding-left: 10px; 
    padding-right: 10px;
    padding-top:10px;
    padding-bottom: 10px;
  
  }
 
  thead {
  background-color: #0f1b27;
  color: rgb(251, 100, 5);
  }
  table {
    width: 100%;
  }

form {
  margin-left: 10px;
  margin-right:10px;
}

#game-form {
  display: none;
}
#hr-line {
  display: none;
  min-width: 145px;
    border: 3px solid linear-gradient(to right, #070323 0%, #d0418e 100%), radial-gradient(circle at top left, #5627cc, #3c3775);
    background-image: linear-gradient(to right, #070323 0%, #d0418e 100%), radial-gradient(circle at top left, #5627cc, #3c3775);
    background-image: -webkit-linear-gradient(to right, #070323 0%, #d0418e 100%), -webkit-radial-gradient(circle at top left, #8252fa, #eca2f1);
    background-image: -moz-linear-gradient(to right, #070323 0%, #d0418e 100%), -moz-radial-gradient(circle at top left, #8252fa, #eca2f1);
    background-image: -ms-linear-gradient(to right, #070323 0%, #d0418e 100%), -ms-radial-gradient(circle at top left, #8252fa, #eca2f1);
    background-image: -o-linear-gradient(to right, #070323 0%, #d0418e 100%), -o-radial-gradient(circle at top left, #8252fa, #eca2f1);
}
</style>

