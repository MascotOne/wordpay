@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6" style="margin:auto; margin-top:20px;">
            <h5 style="padding-top:10px; font-size:20px;">Your Transactions</h5> <hr>
            <table class="table table-dark"  style="background-color: #4608AD;">
                <thead>
                    <th>SN</th>
                    <th>Trans Type</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Date</th>
                </thead>
                @foreach($transactions as $trans)
                <tbody>
                    <tr>
                        <td>{{$sn++}}</td>
                        <td style="width:200px;">{{$trans->transaction_type}}</td>
                        <td style="width:200px;">{{$trans->trans_amount}}</td>
                        @if($trans->trans_status == "Successfull")
                        <td style="width:200px; color:green">{{$trans->trans_status}}</td>
                        @endif
                        @if($trans->trans_status != "Successfull")
                        <td style="width:200px; color: red;">{{$trans->trans_status}}</td>
                        @endif
                        <td style="width:200px;">{{$trans->created_at->toFormattedDateString()}}</td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
</div>
@include('inc.footer')
@endsection
