@extends('layouts.app')

@section('content')
 <div class="container">
     <div class="row">
         <div class="col-md-5" style=" margin:auto;
   margin-top:20px;
     border-radius: 5px;
     box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
    -moz-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
    -webkit-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
    -o-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
    -ms-box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);">
             @if(count($history) > 0)
             <table class="table table-dark" style="background-color: #4608AD;">
                 <thead>
                     <tr>
                         <th scope="col">Word</th>
                         <th scope="col">Played</th>
                         <th scope="col">GP</th>
                         <th scope="col">Stake</th>
                         <th scope="col">Status</th>
                     </tr>
                 </thead>
                 @foreach($history as $his)
                 <tbody>
                     <tr>
                        <td style="width: 100px;">{{$his->enscripted_word}}</td>
                         <td style="width: 100px;">{{$his->word_played}}</td>
                         <td style="width: 100px;">{{$his->game_point}}</td>
                         <td style="width: 100px;">{{$his->played_amount}}</td>
                         @if($his->game_status == "playing")
                               <td><a class="btn btn-info" style="background-color: rgb(236, 14, 68); border:none; color: #fff;" href="{{ route('history-details',$his->id)}}">Playing</a></td>
                         @else($his->game_status == "Ended")
                                @if($his->word_played == $his->correct_word)
                               <td><a  class="btn btn-success" href="{{ route('history-details',$his->id)}}">Won</a></td>
                                @endif
                                @if($his->word_played != $his->correct_word)
                                <td><a  class="btn btn-danger" href="{{ route('history-details',$his->id)}}">Loss</a></td>
                                @endif
                         @endif
                        
                     </tr>
                 </tbody>
                 @endforeach
             </table>
             @else
             <p>No history </p>
             @endif
         </div>
     </div>
 </div>

 @include('inc.footer')
@endsection
