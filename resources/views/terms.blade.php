@extends('layouts.app')


@section('content')
<div class="container" style="margin-top:20px;">
    <div class="row">
        <div class="col-md-5" style="background-color: #fff; margin:auto;">
            <h4>Terms and Conditions</h4> <hr>
            <ul>
              <section>
                The following terms and Conditions govern every user that uses the site.
                <li>Before using the services on these site, you must be atleast 18+.</li>
                <li>piedgames has the right to delete any user if any fraud act is detected on the site</li>
                <li>piedgames is not used to transfer money to others as you will only be able to payout to accounts that carrieds the same name with the one registered on the platform</li>
              </section>
            </ul>

        </div>
    </div>
</div>
@include('inc.footer')

@endsection
