@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3" style="display:none;">
      <h6>Top Winners</h6> <hr>
      <table class="table">
          <thead>
            <tr>
              <th>Player</th>
              <th>Amount Won</th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <td>gfhghsgdhsgdhsgh</td>
            <td>hgsdhgshgd</td>
            </tr>
          </tbody>
      </table>
    </div>
    <div class="col-md-7" style="margin:auto;">
    @include('inc.flash-messages')
    <h4 style="color: #fff;"> Word Game</h4>
    @if(count($game)> 0)
    @foreach($game as $g)
          <div class="card games" style="margin-top: 20px;">
            <table>
              <thead>
                <tr>
                  <th>Instruction</th>
                  <th>Word to find</th>
                  <th>Game Point</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="width:300px;">{{$g->instruction}}</td>
                  <td style="width:250px;"><strong>{{$g->enscripted_word}}</strong></td>
                  <td style="width:250px;">{{$g->final_point}}</td>
                </tr>
              </tbody>
            </table>
            <hr>
            <form action="{{ route('game-play', $g->id)}}" method="POST" >
            @csrf
                <div class="input-group">
                    <input type="text" name="word" class="form-control" placeholder="Enter the correct word">
                    <input type="text" name="stake" class="form-control" placeholder="Stake Amount">
                    <input type="submit" value="Play" class="btn btn-default" style="border:none; background-color: rgb(236, 14, 68); color: #fff;
                    ">
                </div>
            </form>
          </div>


          @endforeach
          @else
          <p>No games avialable as these moment!!</p>
          @endif
       </div>

  </div>
</div>

@include('inc.footer')
@endsection

<style>
  th {
    padding-left: 10px;
    padding-right: 10px;
    padding-top:10px;
    padding-bottom: 10px;
  }
  td {
    padding-left: 10px;
    padding-right: 10px;
    padding-top:10px;
    padding-bottom: 10px;

  }

  thead {
  background-color: #4608AD;
  color: #fff;
  border: none;
  padding: 0 0 0 0;
  }
  table {
    width: 100%;
  }

form {
  margin-left: 10px;
  margin-right:10px;
}

#game-form {
  display: none;
}
#hr-line {
  display: none;
  min-width: 145px;
    border: 3px solid linear-gradient(to right, #070323 0%, #d0418e 100%), radial-gradient(circle at top left, #5627cc, #3c3775);
    background-image: linear-gradient(to right, #070323 0%, #d0418e 100%), radial-gradient(circle at top left, #5627cc, #3c3775);
    background-image: -webkit-linear-gradient(to right, #070323 0%, #d0418e 100%), -webkit-radial-gradient(circle at top left, #8252fa, #eca2f1);
    background-image: -moz-linear-gradient(to right, #070323 0%, #d0418e 100%), -moz-radial-gradient(circle at top left, #8252fa, #eca2f1);
    background-image: -ms-linear-gradient(to right, #070323 0%, #d0418e 100%), -ms-radial-gradient(circle at top left, #8252fa, #eca2f1);
    background-image: -o-linear-gradient(to right, #070323 0%, #d0418e 100%), -o-radial-gradient(circle at top left, #8252fa, #eca2f1);
}
</style>
