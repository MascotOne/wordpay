@extends('layouts.app')


@section('content')
<div class="container" style="margin-top: 20px;">
    <div class="row">
        <div class="col-md-5" style="margin: auto; background-color: #fff;">
            <h4 style="text-align:center;">Help</h4> <hr>
            <ul>
                <li><strong>Game Play</strong></li>
                <section>
                    To play the game, you have to log in into your piedGames account to be able to play the game.Playing is simple all you need is to type in the correct word or quotes and the stake amount in the input box provided and click on the play button to submit your game.
                </section><br>
                <li><strong>What is stake?</strong></li>
                <section>
                  When playing the game you are to submit both the word and stake amount to stand a chance of winning. Stake amount is the amount you want to use in playing the game, You can use from N5 to N1000 to play any game.
                </section><br>
                <li><strong>How is winnings calculated?</strong></li>
                <section>
                  Each time you play a game, Your played word is matched with the correct word and the amount used in playing the game is been multiply by the game point. For instance if the game point as of the time you played the game was 20, and your stake was 1000, then your possible winning is 20 * 1000(Game point * Stake).
                </section><br>
                <li><strong>How to win</strong></li>
                <section>
                    To win the game you have to submit a correct word or quote as instructed.. the result is out immediately the game has ended.
                </section><br>
                <li><strong>How to fund account</strong></li>
                <section>
                    Funding account is simple and fast, you have to login and click on the Fund Account link and select the funding type you want to use. If agents make sure you contact the selected agent with your wordpay account to manually update your playing playing balance.
                </section><br>
                  <li><strong>How do i know when i win?</strong></li>
                  <section>
                    Always check on your history to see the status of your played games. Your history is on the Menu bar under your name when logged in.
                    Also your wordpay account balance is updated, Thats your previous balance + your new winning.
                  </section><br>
                <li><strong>How to payout</strong></li>
                <section>
                    Payout is simple and fast, you can only payout winnings and you must have more than N100 on your winning accounts, if you have more than N100 on your winnings click on the payout button from the menu bar and fill the form.
                    Payout takes some hours after receiving the payout request.
                </section>
            </ul>

        </div>
    </div>
</div>
@include('inc.footer')

@endsection
