@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
        @include('inc.flash-messages')
            <div class="card payout">
                <div class="card-header" style="text-align:center; background-color:#4608AD; color:#fff;">Payout Now -> {{Auth()->user()->account_balance}}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('save-payout') }}">
                        @csrf

                    <input id="fname" type="hidden" class="form-control" name="fname" value="{{Auth()->user()->fname}}" required autofocus>
                    <input id="fname" type="hidden" class="form-control" name="lname" value="{{Auth()->user()->lname}}" required autofocus>
                    <input id="phone_number" type="hidden" class="form-control" name="phone_number" value="{{Auth()->user()->phone_number}}" required autofocus>

                      <div class="form-group row">
                            <div class="col-md-12">
                              <label for="amount">Amount</label>
                                <input id="amount" type="text" class="form-control" name="amount" value="{{Auth()->user()->winnings}}" required autofocus>
                            </div>
                        </div>

                      <div class="form-group row">
                            <div class="col-md-12">
                              <label for="bank_name">Bank Name</label>
                               <select name="bank_name" id="bank_name" class="form-control">
                                   <option value="Wema bank">Wema Bank</option>
                                   <option value="Zenith Bank">Zenith Bank</option>
                                   <option value="Ecobank">Ecobank</option>
                                   <option value="Access bank">Access Bank</option>
                                   <option value="Firstbank">FirstBank</option>
                                   <option value="Gtbank">GTbank</option>
                                   <option value="Stanbic">Stanbic</option>
                                   <option value="federlity bank">Federlity Bank</option>
                               </select>
                            </div>
                        </div>

                         <div class="form-group row">
                            <div class="col-md-12">
                              <label for="account_number">Account Number</label>
                                <input id="account_number" type="text" class="form-control" name="account_number" placeholder="Please enter your account number" required autofocus>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-default" style="background-color: rgb(236, 14, 68); color: #fff;">
                                  Payout Now
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.footer')
@endsection

<style media="screen">
  .payout {
    margin:auto;
    margin-top:20px;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: -1px 1px 20px 13px rgba(7, 18, 247, 0.15);
  }
</style>
