@extends('layouts.admin-nav')


@section('content')
<div class="container-fluid">
<div class="row">
  @include('inc.admin-header')
<div class="col-md-9 pending-payout">
  @include('inc.flash-messages')
    <h4 style="padding-top:10px;">Pending Payouts</h4>
    @if(count($pendingPayouts) > 0)
    <table class="table table-dark">
        <thead>
            <th>SN</th>
            <th>Fullname</th>
            <th>Phone</th>
            <th>Amount</th>
            <th>Bank Name</th>
            <th>Account Number</th>
            <th>Status</th>
            <th>Date</th>
            <th>Actions</th>
        </thead>
        @foreach($pendingPayouts as $payout)
        <tbody>
            <tr>
                <td>{{$i++}}</td>
                <td style="width: 170px;">{{$payout->fname}} {{$payout->lname}}</td>
                <td  style="width: 170px;">{{$payout->phone_number}}</td>
                <td  style="width: 170px;">{{$payout->payout_amount}}</td>
                <td  style="width: 170px;">{{$payout->bank_name}}</td>
                <td  style="width: 170px;">{{$payout->account_number}}</td>
                <td  style="width: 170px;">{{$payout->status}}</td>
                <td  style="width: 170px;">{{$payout->created_at->toFormattedDateString()}}</td>
                <td  style="width: 170px;">
                    <form action="{{ route('update-payout', $payout->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="_method" value="PUT">
                    <input type="submit" class="btn btn-success" value="Update" name="submit">
                    </form>
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>

</div>
@else
<p style="text-align:center;">Wow is like all players has been paid and no pending payout for now!!!!!</p>
@endif
</div>
</div>
@endsection

<style media="screen">
  .admin-sidebar {
    height: 657px;
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
  }

  thead {
    background-color: #070225;
    color: #fff;
  }

  .table thead th {
    border: 1px solid #070225;
  }

.pending-payout {
  height: 600px;
  overflow-y: scroll;
  margin-left: 20px;
  margin-top: 40px;
  background-color: #fff;
  box-shadow: 0px 12px 20px 0px #210aa2;
  border-radius: 5px;
}


</style>
