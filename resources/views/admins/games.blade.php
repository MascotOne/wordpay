@extends('layouts.admin-nav')

@section('content')
		<div class="container-fluid">
			<div class="row">
					@include('inc.admin-header')
						@if(count($games)>0)
			      	<div class="col-md-9 games">
				    		@include('inc.flash-messages')
			          	<div class="forms" style="margin-top:20px;">
										<form action="{{ route('start-all')}}" method="POST" style="float:left;">
												@csrf
												@method('PUT')
												<input type="submit" class="btn btn-primary" style="background-color: blue; border: none;" value="Start All">
												<input name="_method" type="hidden" value="PUT"/>

											</form>

											<form action="{{ route('end-all')}}" method="POST" style="float:right;">
												@csrf
												@method('PUT')
												<input type="submit" class="btn btn-danger" value="End All">
												<input name="_method" type="hidden" value="PUT"/>

											</form>
										</div>
										<hr>
										<h4 style="padding-top:30px;">Games</h4>
										<hr>

										<table class="table">
											<thead>
												<tr>
														<th>Enscripted Word</th>
														<th>Time(Start - End)</th>
                            <th>Status</th>
                            <th>Date</th>
														<th>Actions</th>
														<th>Control</th>
												</tr>
											</thead>

					                        @foreach($games as $game)
												<tbody>
													<tr>
														<td style="width:200px;">{{$game->enscripted_word}}</td>
                            <td  style="width:200px;">{{$game->start_time}} - {{$game->end_time}}</td>
                            <td  style="width:200px;">{{$game->game_status}}</td>
                            <td  style="width:200px;">{{$game->created_at->toFormattedDateString()}}</td>
                            <td  style="width:200px;">
                                <a href="{{ route('game-details', $game->id)}}"> <button class="btn btn-default">Manage</button> </a>
														</td>
														@if($game->game_status == "playing")
														<td  style="width:200px;">
															<form action="{{ route('end-game', $game->id)}}" method="POST">
																@csrf
																<input type="hidden" name="_method" value="PUT">
																<input type="submit" class="btn btn-danger" value="End Game">
															</form>
													   </td>
													   @elseif($game->game_status == "Pending")
													   <td  style="width:200px;">
															<form action="{{ route('start-game', $game->id)}}" method="POST">
																@csrf
																<input type="hidden" name="_method" value="PUT">
																<input type="submit" class="btn btn-success" style="background-color: blue; border: none;" value="Start Game">
															</form>
													   </td>
													   @else
													   <td  style="width:200px;">Game Ended</td>
													   @endif
													</tr>
					                            </tbody>
					                            @endforeach
										</table>
									</div>
				@else
					<div class="alert alert-danger" style="width:100%; margin-top:20px;">
 						<p>No games available!! <a class="btn btn-default" href="">add Game</a></p>
					</div>
				@endif
			</div>
		</div>
@endsection

<style media="screen">
  .admin-sidebar {
    height: 657px;
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
  }

	thead {
	  background-color: #070225;
	  color: #fff;
	}

	.table thead th {
	  border: 1px solid #070225;
	}

.games {
	height: 600px;
	overflow-y: scroll;
	margin-left: 20px;
	margin-top: 40px;
	background-color: #fff;
	box-shadow: 0px 12px 20px 0px #210aa2;
	border-radius: 5px;
}


</style>
