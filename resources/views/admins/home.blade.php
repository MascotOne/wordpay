@extends('layouts.admin-nav')

@section('content')
 <div class="container-fluid details" style="margin-top:1px;">
    <div class="row">
    @include('inc.admin-header')
    <div class="col-md-9 admin-home">
      <h5>Admin Dashboard</h5>
    	@if(count($games)> 0)
      <table class="table">
            <thead>
              <tr>
                  <th>Enscripted Word</th>
                  <th>Time(Start - End)</th>
                  <th>Status</th>
                  <th>Date</th>
                  <th>Actions</th>
              </tr>
            </thead>

                        @foreach($games as $game)
              <tbody>
                <tr>
                  <td style="width:150px;">{{$game->enscripted_word}}</td>
                  <td style="width:150px;">{{$game->start_time}} - {{$game->end_time}}</td>
                  <td style="width:150px;">{{$game->game_status}}</td>
                  <td>{{$game->created_at->toFormattedDateString()}}</td>
                  <td style="width:150px; float:right;">
                      <a href="{{ route('game-details', $game->id)}}"> <button class="btn btn-success" style="background-color: blue; border: none;">View Details</button> </a>
                  </td>
                </tr>
                            </tbody>
                            @endforeach
          </table>
    @else
    <p style="color:#fff; text-align:center;">No Active game now!!</p>
    @endif
</div>
    </div>
  </div>
@endsection
<style media="screen">
  .admin-sidebar {
    height: 658px;
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
  }
.admin-home {
  height: 500px;
  margin-left: 20px;
  margin-top: 40px;
  background-color: #fff;
  box-shadow: 0px 12px 20px 0px #210aa2;
  border-radius: 5px;
  overflow-y: scroll;
}
thead {
  background-color: #070225;
  color: #fff;
}

.table thead th {
  border: 1px solid #070225;
}
</style>
