@extends('layouts.admin-nav')


@section('content')
<div class="container-fluid">
	<div class="row">
			@include('inc.admin-header')
			<div class="col-md-9 players">
				@include('inc.flash-messages')
	      <h4>Players ({{ count($players)}})</h4>
	    	@if(count($players)> 0)
	      <table class="table">
	            <thead>
	              <tr>
								   	<th>SN</th>
										<th>Full name</th>
										<th>Email</th>
										<th>Joined</th>
										<th>Account</th>
										<th>Update</th>
										<th>Modify</th>
										<th>Del</th>
	              </tr>
	            </thead>

	                        @foreach($players as $player)
	              <tbody>
	                <tr>
										<td>{{$i++}}</td>
			  							<td>{{$player->fname}} {{$player->lname}}</td>
			  							<td>{{$player->email}}</td>
			 							<td>{{ $player->created_at->toFormattedDateString() }}</td>
										<td>{{ $player->account_balance }}</td>
			 							<td>
			 							 <form action="{{ route('editPlay-balance', $player->id)}}" method="POST">
			 							     	@csrf
			 									 @method('PUT')
			 									 <div class="input-group">
			 									 <input name="_method" type="hidden" value="PUT"/>
			 									 <input type="text" name="play_balance" placeholder="{{$player->play_balance}}" style="width:60px;"/>
			 									 <input type="submit" class="btn btn-primary" value="Credit">
			 									 </div>

			 								</form>
			 							 </td>
			 							 <td>
			 								<form action="{{ route('make-admin', $player->id)}}" method="POST">
			 							     	@csrf
			 									 @method('PUT')
			 									 <input name="_method" type="hidden" value="PUT"/>
			 									<input type="submit" class="btn btn-success" value="Make Admin">
			 								</form>
			 							</td>

			 							<td>
			 								<form action="{{ route('delete-player', $player->id)}}" method="POST">
			 							     	@csrf
			 									 @method('DELETE')
			 									 <input name="_method" type="hidden" value="DELETE"/>
			 									<input type="submit" class="btn btn-danger" value="Del">
			 								</form>
			 							</td>
			 						</tr>

	                            </tbody>
	                            @endforeach
	          </table>
	    @else
	    <p style="color:#fff; text-align:center;">No Active game now!!</p>
	    @endif
	</div>

	</div>
</div>
@endsection

<style media="screen">
  .admin-sidebar {
    height: 657px;
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
  }

	thead {
	  background-color: #070225;
	  color: #fff;
	}

	.table thead th {
	  border: 1px solid #070225;
	}

.players {
	height: 600px;
	overflow-y: scroll;
	margin-left: 20px;
	margin-top: 40px;
	background-color: #fff;
	box-shadow: 0px 12px 20px 0px #210aa2;
	border-radius: 5px;
}


</style>
