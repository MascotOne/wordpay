@extends('layouts.admin-nav')


@section('content')
    <div class="container-fluid">
      <div class="row">
          @include('inc.admin-header')
            <div class="col-md-9  game-details">
                @include('inc.flash-messages')
                <h5 style="padding-top:20px; padding-bottom:20px;">Details for ( {{$game->enscripted_word}} - {{$game->correct_word}} ) {{ $game->instruction}}</h5> <hr>
                <section>
                    <span style="font-wieght:10px;">Enscripted Word </span>
                    <span style="float:right;"> <strong>{{$game->enscripted_word}}</strong></span> <br>
                    <span>correct Word </span>
                    <span style="float:right;">  <strong>{{$game->correct_word}}</strong></span> <br>

                    <span style="font-wieght:10px;"> Game Time</span>
                    <span style="float:right;"> <strong>{{$game->start_time}} - {{$game->end_time}}</strong></span> <br>

                    <span style="font-wieght:10px;"> Duration</span>
                    <span style="float:right;"> <strong>{{$game->duration}}</strong></span> <br>

                    <span style="font-wieght:10px;"> Game Date</span>
                    <span style="float:right;"> <strong>{{$game->created_at->toFormattedDateString()}}</strong></span> <br>

                    <span style="font-wieght:10px;"> Game Status</span>
                    <span style="float:right;"> <strong>{{$game->game_status}}</strong></span> <br>

                     <span style="font-wieght:10px;"> Starting Point</span>
                    <span style="float:right;"> <strong>{{$game->initial_point}}</strong></span> <br>

                    <span style="font-wieght:10px;"> Final or Playing Point</span>
                    <span style="float:right;"> <strong>{{$game->final_point}}</strong></span> <br>

                     <span style="font-wieght:10px;"> Created By</span>
                    <span style="float:right;"> <strong>{{$game->admin_name}} (Admin)</strong></span> <br>

                    <hr>
                    <span> <strong>Meaning</strong></span>
                    <p>{{$game->meaning}}</p>
                    <span> <strong>Total Players</strong> {{ count($players) }}</span>

                    <br>


                    @if($game->game_status == "Pending")
                    <form action="{{ route('start-game', $game->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="_method" value="PUT">
                        <input type="submit" value="Start Game" class="btn btn-success">
                    </form>
                    @elseif($game->game_status == "playing")
                    <form action="{{ route('end-game', $game->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="_method" value="PUT">
                        <input type="submit" value="End Game" class="btn btn-danger">
                    </form>
                    @endif
                </section>


                @if(count($winners) > 0)
                     <h5 style="padding-top:20px; padding-bottom:20px;">Winners({{$countWinners}})</h5> <hr>
                     <section>
                         <table class="table">
                             <thead>
                                 <tr>
                                 <th>SN</th>
                                 <th>Name</th>
                                 <th>Word Played</th>
                                 <th>Stake</th>
                                 <th>Amount Won</th>
                                 <th>Modify</th>
                                 </tr>
                             </thead>
                             @foreach($winners as $winner)
                             <tbody>
                                 <td>{{$sn++}}</td>
                                 <td style="width:200px;">{{$winner->player_fname}} {{$winner->player_lname}}</td>
                                 <td style="width:200px;">{{$winner->word_played}}</td>
                                 <td style="width:200px;">{{$winner->played_amount}}</td>
                                 <td style="width:200px;">{{$winner->played_amount * $winner->game_point}} </td>
                                
                                 </tbody>
                             @endforeach
                         </table>
                       </section>
                         @else
                       <h1 style="text-align:center;">OOPs no winner</h1>
                       @endif
                     </div>

            </div>
    </div>
@endsection

<style media="screen">
  .admin-sidebar {
    height: 100vh;
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
  }

thead {
  background-color: #070225;
  color: #fff;
}

.table thead th {
  border: 1px solid #070225;
}

.game-details {
  height: 600px;
  overflow-y: scroll;
  margin-left: 20px;
  margin-top: 40px;
  background-color: #fff;
  box-shadow: 0px 12px 20px 0px #210aa2;
  border-radius: 5px;
}

</style>
