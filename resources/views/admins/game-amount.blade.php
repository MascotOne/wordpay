@extends('layouts.admin-nav')

@section('content')
<div class="container" style="margin-top:20px;">
@include('inc.admin-header')
    <div class="row">
        <div class="col-md-5 add-message">
        <h4 style="text-align:center; color:#000;">Add Game Amount</h4>
            <form action="{{ route('save-amount')}}" method="POST">
                @csrf
                <input type="text" name="amount" class="form-control" placeholder="200">
                <input type="submit" name="submit" class="btn btn-default" value="Save">
            </form>
        </div>
    </div>
</div>


@endsection