@extends('layouts.admin-nav')

@section('content')
<div class="container" style="color:#000;">
    @include('inc.admin-header')
    <div class="row">
        <div class="col-md-6 messages">
            <h4>Display Messages</h4>
            @if(count($displayMessages) > 0)
           <table class="table-responsive">
               <thead>
                   <tr>
                       <th>SN</th>
                       <th>Message</th>
                       <th>Date</th>
                       <th>Actions</th>
                   </tr>
               </thead>
               <tbody>
                   @foreach($displayMessages as $message)
                   <tr>
                       <td>{{$sn++}}</td>
                       <td style="width:500px;">{{$message->message}}</td>
                       <td style="width: 100px;">{{$message->created_at->toFormattedDateString()}}</td>
                       <td>
                           <form action="{{ route('delete-message', $message->id)}}" method="POST">
                               @csrf
                               @method('DELETE')
                               <input type="hidden" name="_method" value="DELETE">
                               <input type="submit" value="Del" class="btn btn-danger">
                           </form>
                       </td>
                   </tr>
                   @endforeach
               </tbody>
           </table>
            @else 
            <p>No display messages</p>
            @endif
        </div>
    </div>
</div>

@endsection