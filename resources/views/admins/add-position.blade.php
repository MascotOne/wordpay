@extends('layouts.admin-nav')

@section('content')
<div class="container" style="margin-top:20px;">
@include('inc.admin-header')
    <div class="row">
    @include('inc.flash-messages')
        <div class="col-md-5 add-message">
        <h4 style="text-align:center; color:#000;">Add position</h4>
        <hr>
            <form action="{{ route('save-position')}}" method="POST">
                @csrf
                <div class="form-group row">
                            <div class="col-md-6">
                            <label for="position">Position</label>
                            <input id="position"  type="text" class="form-control" name="position" placeholder="1 - 3" required>
                            </div>

                            <div class="col-md-6">
                            <label for="odd">Odds</label>
                            <input id="odd"  type="text" class="form-control" name="odd" required>
                            </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                    <input type="submit" name="submit" class="btn btn-default" value="Save">
                    </div>
                </div>
              
            </form>
        </div>
    </div>
</div>


@endsection