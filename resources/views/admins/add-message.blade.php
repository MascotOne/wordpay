@extends('layouts.admin-nav')

@section('content')
<div class="container" style="margin-top:20px;">
@include('inc.admin-header')
    <div class="row">
        <div class="col-md-5 add-message">
        <h4 style="text-align:center; color:#000;">Add Display Message here</h4>
            <form action="{{ route('save-message')}}" method="POST">
                @csrf
                <input type="text" name="message" class="form-control" placeholder="Enter display message">
                <input type="submit" name="submit" class="btn btn-default" value="Save">
            </form>
        </div>
    </div>
</div>


@endsection