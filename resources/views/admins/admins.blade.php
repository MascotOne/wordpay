@extends('layouts.admin-nav')


@section('content')
<div class="container-fluid">
	<div class="row">
		@include('inc.admin-header')
		<div class="col-md-9 admins">
		<h4 style="padding-top: 20px;">Admins ({{$countAdmins}})</h4>
		<hr>
			<table class="table">
				<thead>
					<tr>
 						<th>SN</th>
 						<th>Full name</th>
 						<th>Email</th>
 						<th>Phone Number</th>
 						<th>Member since</th>
 						<th>Actions</th>
					</tr>
					@foreach($admins as $admin)
					<tbody>
						<tr>
 							<td>{{$i++}}</td>
 							<td style="width:200px;">{{$admin->fname}} {{$admin->lname}}</td>
 							<td style="width:200px;">{{$admin->email}}</td>
 							<td style="width:200px;">{{$admin->phone_number}}</td>
 							<td style="width:200px;">{{$admin->created_at->toFormattedDateString()}}</td>
							<td style="width:200px;">
								<form action="{{ route('make-player', $admin->id)}}" method="POST">
								    @csrf
									 @method('PUT')
									 <input name="_method" type="hidden" value="PUT">
									<input type="submit" class="btn btn-success" value="Make Player">
								</form>
							</td>

							<td>
								<form action="{{ route('delete-admin', $admin->id)}}" method="POST">
								    @csrf
									 @method('DELETE')
									 <input name="_method" type="hidden" value="DELETE">
									<input type="submit" class="btn btn-danger" value="Del">
								</form>
							</td>
						</tr>
					</tbody>
					@endforeach
				</thead>
			</table>

		</div>
	</div>
</div>
@endsection

<style media="screen">
	.admin-sidebar {
		height: 657px;
		background-color: #fff;
		box-shadow: 0px 12px 20px 0px #210aa2;
	}

	thead {
		background-color: #070225;
		color: #fff;
	}

	.table thead th {
		border: 1px solid #070225;
	}

.admins {
	height: 600px;
	overflow-y: scroll;
	margin-left: 20px;
	margin-top: 40px;
	background-color: #fff;
	box-shadow: 0px 12px 20px 0px #210aa2;
	border-radius: 5px;
}


</style>
