@extends('layouts.admin-nav')

@section('content')
<div class="container-fluid">
        <div class="row">
            @include('inc.admin-header')
        <div class="col-md-6 add-game">
              @include('inc.flash-messages')
                <h4>Add Game</h4>
                    <form method="POST" action="{{ route('save-word') }}">
                        @csrf

                         <div class="form-group row">
                            <div class="col-md-6">
                              <label for="word">Enscripted Word</label>
                                <input id="word" type="text" class="form-control"  name="enword"  required autofocus>
                            </div>

                            <div class="col-md-6">
                              <label for="correct_word">Correct Word</label>
                                <input id="correct_word" type="text" class="form-control"  name="correct_word"  required autofocus>
                            </div>

                              <div class="col-md-6" style="margin-top:10px;">
                            <label for="game_point">Game Point</label>
                                <input id="game_point" type="text" class="form-control" placeholder="0" name="game_point" required>
                            </div>

                              <div class="col-md-6" style="margin-top:10px;">
                            <label for="game_type">Game Type</label>
                               <select name="game_type" id="game_type" class="form-control">
                                   <option value="words">Words</option>
                                   <option value="quotes">Quotes</option>
                               </select>
                            </div>

                            <div class="col-md-12" style="margin-top:10px;">
                            <label for="instruction">Instruction</label>
                                <input id="instruction" type="text" class="form-control" name="instruction" required>
                            </div>
                        </div>




                          <div class="form-group row">
                            <div class="col-md-12" >
                              <label for="meaning">Meaning/Defination</label>
                              <textarea name="meaning" id="meaning" class="form-control"  required  cols="10" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                            <label for="start_time">start Time</label>
                                <input id="start_time" type="time" class="form-control" name="start_time" required>
                            </div>

                            <div class="col-md-4">
                            <label for="end_time">End Time</label>
                                <input id="end_time" type="time" class="form-control" name="end_time" required>
                            </div>

                             <div class="col-md-4">
                            <label for="duration">Duration</label>
                                <input id="duration" type="text" class="form-control" name="duration" required>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-default">
                                    {{ __('Save Game') }}
                                </button>
                            </div>
                        </div>
                    </form>
        </div>

    </div>
</div>

@endsection

<style media="screen">
  .admin-sidebar {
    height: 657px;
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
  }

  .add-game {
    margin-top: 40px;
    margin-left: 20px;
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
    height: 600px;
    overflow-y: scroll;
    margin: auto;
    border-radius: 5px;
  }



</style>
