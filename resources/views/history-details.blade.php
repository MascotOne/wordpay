@extends('layouts.app')

@section('content')
 <div class="container">
 <a class="btn btn-default" style="margin-top:20px; margin-left:310px;  background-color: #fff; border:none; color: #000;" href="{{ route('history', Auth()->user()->id)}}">Back to history</a>
     <div class="row">
         <div class="col-md-5" style=" margin:auto; margin-top:20px; background-color: #07074c; border-radius: 6px;">
           <h4 style="text-align:center; color: #fff;">Game Details</h4>
            <p style="color: white; padding-top: 30px;">
                <span>Winning Status :</span>
                @if($historyDetails->game_status == "playing")
                 <span style="color: white; float: right;">Playing</span> <br>
                @if($historyDetails->game_status == "Ended")
                @if($historyDetails->word_played == $historyDetails->correct_word)
                <span style="color: green; float: right;">Won</span> <br>
                @endif
                @endif
                @endif
                @if($historyDetails->game_status == "Ended")
                @if($historyDetails->word_played != $historyDetails->correct_word)
                <span style="color: red; float: right">Loss</span> <br>
                @endif
                @endif
                <span>Enscripted Word :</span>
                <span style="float: right;">{{$historyDetails->enscripted_word}}</span>
                <br>
                <span>Played Word :</span>
                <span style="float: right;">{{$historyDetails->word_played}}</span>
                    <br>
                <span>Stake Amount :</span>
                <span style="float: right;">{{$historyDetails->played_amount}}</span>
                <br>
                <span>Game Status :</span>
                <span style="float: right;">{{$historyDetails->game_status}}</span>
                <br>
                <span>Game Point :</span>
                <span style="float: right;">{{$historyDetails->game_point}}</span>
                <br>
                @if($historyDetails->game_status == "Ended")
                @if($historyDetails->word_played == $historyDetails->correct_word)
                <span>Amount Won : </span>
                <span style="float: right;">{{$historyDetails->played_amount * $historyDetails->game_point}}</span> <br>
                @endif
                @endif
                @if($historyDetails->game_status == "Ended")
                @if($historyDetails->word_played != $historyDetails->correct_word)
                <span>Possible Winnings :</span>
                <span style="float: right;">0.0</span> <br>
                @endif
                @endif
                @if($historyDetails->game_status == "playing")
                <span>Possible Winnings :</span>
                <span style="float: right;">{{$historyDetails->played_amount * $historyDetails->game_point}}</span> <br>
                <hr>
                @endif
                <span>Game Type :</span>
                <span style="float: right;">{{$historyDetails->game_type}}</span>

            </p>
         </div>
     </div>
 </div>
 @include('inc.footer')
@endsection
