@extends('layouts.auth-nav')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card login">
                <div class="card-header" style="text-align:center; border:none; color:#fff; background-color: #4608AD;">Login to your account!</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                            <label for="email">Email</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                            <label for="password">Password</label>
                                <input id="password"  type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-default" style="background-color:rgb(236, 14, 68); border:none; color:#fff;">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link"  href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                <hr>
                               <p>Don't have an account? <a href="{{ route('register')}}">Register</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
</div>
        </div>
@include('inc.footer')
@endsection

<style media="screen">
  .login {
     margin:auto;
     margin-top:20px;
     background-color: #fff;
    border-radius: 5px;
    box-shadow: 1px 1px 20px 8px rgba(103, 26, 199, 0.15);
  }
</style>
