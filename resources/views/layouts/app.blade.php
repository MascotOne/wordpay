<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page_title }}</title>
    <meta name="description" content="{{$page_description}}">
    <meta name="keywords" content="{{$page_keywords}}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/main.js')}}"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body onLoad="hideLoader()">
    <div id="app">
        <nav class="navbar navbar-expand-xl  navbar-light navbar-laravel" style="background-color:#4608AD;">
            <div class="container">
                @guest
                <a class="navbar-brand" style="color: #fff; font-family:arial black;" href="{{ url('/') }}">
                    {{ config('app.name', 'wordPay') }}
                </a>
                @else
                <a class="navbar-brand" style="color: #fff; font-family:arial black;" href="{{ route('home') }}">
                    {{ config('app.name', 'wordPay') }}
                </a>
                @endguest

                <button style="color:#fff;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                  Menu
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    @guest

                    <li class="nav-item"> <a class="nav-link" style="color: #fff;; font-family:arial black;" href="{{ route('index') }}">Home</a> </li>
                    <li class="nav-item">
                                <a class="nav-link btn btn-default" href="{{ route('fund-account')}}" style="color: #fff; font-family:arial black; border: none; background-color:rgb(236, 14, 68);">Fund Account</a>
                   </li>
                    @else
                   <li class="nav-item"> <a class="nav-link" style="color: #fff; font-family:arial black;" href="{{ route('home') }}">Home</a> </li>
                   <li class="nav-item">
                                <a class="nav-link btn btn-default" href="{{ route('fund-account')}}" style="color: #fff; font-family:arial black; background-color: rgb(236, 14, 68); border:none;">Fund Account</a>
                   </li>
                   @endguest
                 </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Authentication Links -->
                        @guest

                            <li class="nav-item">

                            <a class="nav-link" style="color: #fff;" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" style="color: #fff; font-family:arial black;" href="{{ route('register') }}"> <i class="fas fa-user-plus"></i> {{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link btn btn-default" href="{{ route('payout')}}" style="color: #fff; font-family:arial black; background-color:rgb(236, 14, 68); border:none;">Payout</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" style="color: #fff; font-family:arial black;" >Account Balance:&#8358 {{ Auth()->user()->account_balance}}</a>
                            </li>



                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" style="color: #fff; font-family:arial black;" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->lname }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('history', Auth()->user()->id) }}">
                                        History
                                    </a>

                                     <a class="dropdown-item" href="{{ route('user-trans', Auth()->user()->id) }}">
                                       Transactions
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="main">
    @yield('content')
    <div id="loader"></div>
    </div>
</body>
</html>

<style>
  body {
    background-image: url('/storage/background.png');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    background-color: #030f44;
  }
</style>
