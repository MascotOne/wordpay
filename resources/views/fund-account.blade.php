@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card fund-account" style="background-color: #fff; color: #676651;">
                <div class="card-header" style="padding-top:20px; text-align:center; background-color: #4608AD; text-align:center; color: #fff;">Fund your account now to start playing!</div>

                <div class="card-body">
                   <h3>Through Agents</h3> <hr>
                   <div class="alert alert-primary"> To fund your account, you can use our trusted agents below to fund your wordPay account
                       After sending or making a deposit to any of the agent's account please contact the agent with the following details. <br>
                       <span>Depositor's name</span> <br>
                       <span>Your wordPay email</span> <br>
                       <span>Amount Deposited</span>

                   </div>

                       <br>
                       <section>
                       <span> <strong>Gideon Gizamma </strong></span> <br>
                       <span>Account Number: 207335383</span> <br>
                       <span>Bank name: Zenith Bank</span> <br>
                       <span>State: Edo State</span> <br>
                       <span>Phone Number: 0909876544</span> <br>
                       <span>Email: gideon@wordpay.com</span>
                       </section>
                       <br>
                        <section>
                       <span> <strong>Nsisong Eduok </strong></span> <br>
                       <span>Account Number: 207335383</span> <br>
                       <span>Bank name: Zenith Bank</span> <br>
                       <span>State: Akwa Ibom State</span>
                       <span>Phone Number: 0909876544</span>
                       <span>Email: nee@wordpay.com</span>
                       </section>
                      <br>
                       <section>
                       <span> <strong>Dafe Proven </strong></span> <br>
                       <span>Account Number: 207335383</span> <br>
                       <span>Bank name: Zenith Bank</span> <br>
                       <span>State: Delta State</span>
                       <span>Phone Number: 0909876544</span>
                       <span>Email: proven@wordpay.com</span>
                       </section>

                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.footer')
@endsection
