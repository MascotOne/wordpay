<div class="col-md-2 admin-sidebar">
   <h4 style="text-align:center;"> <a style="text-decoration: none;" href="{{ route('home')}}"> Dashboard</a></h4> <hr>
   <div class="alert alert-success" style="background-color: #fff; box-shadow: 0px 12px 20px 0px #210aa2;" >
       <form action="{{route('find-users')}}" method="GET">
        @csrf
        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Search for users" name="find_user"> 
                            </div>
                        </div>
       </form>
      </div>
   <ul style="list-style-type: none;">
     <li style="padding-top: 20px;" ><a style="text-decoration: none; color: gray; font-size: 15px;"  href="{{ route('add-word')}}">Add Game</a></li>
     <li style="padding-top: 20px;" ><a style="text-decoration: none; color: gray; font-size: 15px;"  href="{{ route('games')}}">Games</a></li>
     <li style="padding-top: 20px;" ><a style="text-decoration: none; color: gray; font-size: 15px;"  href="{{ route('players')}}">Players</a></li>
     <li style="padding-top: 20px;" ><a style="text-decoration: none; color: gray; font-size: 15px;"  href="{{ route('admins')}}">Admins</a></li>
     <li style="padding-top: 20px;" ><a style="text-decoration: none; color: gray; font-size: 15px;"  href="{{ route('payouts')}}">Payouts</a></li>
     <li style="padding-top: 20px;" ><a style="text-decoration: none; color: gray; font-size: 15px;"  href="{{ route('pending-payouts')}}">Pending Payout</a></li>
     <hr>
     <li>
       <a class="btn btn-info" style="background-color: blue; border: none; color: #fff;" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
           {{ __('Logout') }}
       </a>

       <form id="logout-form" action="{{ route('logout') }}" method="POST">
           @csrf
       </form>
     </li>

   </ul>
</div>

<style>
.search {
    background-color: #fff;
    box-shadow: 0px 12px 20px 0px #210aa2;
}
</style>
