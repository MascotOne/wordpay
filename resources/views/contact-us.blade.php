@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 20px;">
    <div class="row">
        <div class="col-md-5" style="background-color: #fff; margin: auto;">
            <h4>We like to hear from you</h4> <hr>
            <p>We are open 24 hours a day and 7 days a week to assist you with the game issues.</p>
            <ul>
                <li><strong>For Fundings </strong></li>
                <section>
                    Phone Number: +234 646466464646 <br>
                    Email: fundings@wordpay
                </section>

                <li><strong>For Payouts </strong></li>
                <section>
                    Phone Number: +234 646499646 <br>
                    Email: payouts@wordpay
                </section>

                <li><strong>For Others </strong></li>
                <section>
                    Phone Number: +234 646466464646 <br>
                    Email: info@wordpay
                </section>
            </ul>
        </div>
    </div>
</div>
@include('inc.footer')
@endsection