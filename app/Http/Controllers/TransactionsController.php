<?php

namespace App\Http\Controllers;
use App\Transactions;
use Carbon\carbon;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userTrans($id) {
        $page_title = "Your transactions | wordPay";
        $page_description = "Playing online games for money";
        $page_keywords   = "wordpay, online games, betting online, online money, making money online";
        $transactions = Transactions::where('user_id', '=', $id)->orderBy('id','DESC')->get();
        $sn = 1;
        return view('transactions')->with('page_title', $page_title)
                                   ->with('page_description', $page_description)
                                   ->with('page_keywords', $page_keywords)
                                   ->with('sn', $sn)
                                   ->with('transactions', $transactions);
    }
}
