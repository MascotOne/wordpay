<?php

namespace App\Http\Controllers;
use App\Payout;
use App\Transactions;
use App\User;
use DB;
use Illuminate\Http\Request;

class PayoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function index() {
    $page_title = "Payout your winnings | wordPay";
    $page_description = "wordPay is an online gaming platform where players can use as litle as 100 naira to predict words and make 100 of thousands or even millions after getting the right word";
    $page_keywords = "play games, online games, how to make money online, making money online, sure way of making money in Nigeria, make money in nigeria";
    return view('payout')->with('page_title', $page_title)
                        ->with('page_description', $page_description)
                        ->with('page_keywords', $page_keywords);
}

public function savePayout(Request $request) {
    $this->validate($request, [
     'fname' => 'required|max:30',
     'lname' => 'required|max:30',
     'phone_number' => 'required',
     'bank_name'    => 'required',
     'amount' => 'required|numeric',
     'account_number' => 'required|numeric',
    ]);
    $user_id = Auth()->user()->id;
    $accountBalance = Auth()->user()->account_balance; // Get users current account balance amount

    if($accountBalance < $request->input('amount')) {
        return redirect()->back()->with('error', 'Insuficient fund to payout');
    } else {

    $newBalance = Auth()->user()->account_balance - $request->input('amount'); // Subtract the payout amount from the previous amount
    $user_id = Auth()->user()->id; // Get the user logged in to update winnings;
    $updateUserBalance = User::find($user_id); // Find the user to update by id
    $updateUserBalance->account_balance = $newBalance;
    $updateUserBalance->update();

    $payout = new Payout;
    $payout->fname = $request->input('fname');
    $payout->lname = $request->input('lname');
    $payout->phone_number = $request->input('phone_number');
    $payout->payout_amount = $request->input('amount');
    $payout->user_id = $user_id;
    $payout->bank_name = $request->input('bank_name');
    $payout->account_number = $request->input('account_number');
    $payout->status = "Pending";
    
    $payout->save();
   
    $transaction = new Transactions;
    $transaction->fname = Auth()->user()->fname;
    $transaction->lname = auth()->user()->lname;
    $transaction->transaction_type = "payout";
    $transaction->user_id = Auth()->user()->id;
    $transaction->trans_amount = $request->input('amount');
    $transaction->trans_status = "pending";
    $transaction->trans_id = "Payout".rand();
    $transaction->save();
    return redirect()->back()->with('success', 'Payout successfull');
}
   
   
}

}
