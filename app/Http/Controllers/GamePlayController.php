<?php

namespace App\Http\Controllers;
use App\GamePlay;
use App\Game;
use App\User;
use DB;
use App\winners;
use Illuminate\Http\Request;

class GamePlayController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function gamePlay(Request $request,$id) {
             $this->validate($request, [
              'word' => 'required',
              'stake' => 'required'
            ]);
            $accountBalance = Auth()->user()->account_balance;
            $player_id = Auth()->user()->id;
            // These is the highest amount that can be staked
            $amountToStake = 1000;
            $playersStake = $request->input('stake');
            // If staked amount is more than the amountToStake reject
            if($playersStake > $amountToStake) {
                return redirect()->back()->with('error', 'Maximum staking amount is 1000');
            }
            if($accountBalance < 5) {
              return redirect()->back()->with('error', 'Insuficient fund to play game.');
            }
            if($playersStake > $accountBalance) {
              return redirect()->back()->with('error', 'Insuficient fund to play game');
            }
            if($playersStake == 0) {
                return redirect()->back()->with('error', 'Invalid stake amount');
            }
             else {
                $game = Game::find($id);
                $gamePlay = new GamePlay;
                $gamePlay->word_played = $request->input('word');
                $gamePlay->correct_word = $game->correct_word;
                $gamePlay->enscripted_word = $game->enscripted_word;
                $gamePlay->player_fname = Auth()->user()->fname;
                $gamePlay->player_lname = Auth()->user()->lname;
                $gamePlay->player_id = Auth()->user()->id;
                $gamePlay->player_number = Auth()->user()->phone_number;
                $gamePlay->game_id = $game->id;
                $gamePlay->played_amount = $request->input('stake');
                $gamePlay->marked        = "Unmarked";
                $gamePlay->game_point    = $game->final_point;
                $gamePlay->game_type     = $game->game_type;
                $gamePlay->game_status   = "playing";
                $gamePlay->save();
                $userAccount = Auth()->user()->account_balance;
                $newAccountBalance = $userAccount - $request->input('stake');
                $removeMoney = DB::update("UPDATE users SET account_balance='$newAccountBalance' WHERE id='$player_id'");

                // Check how many players have played the game.
                $gameId = $id;
                $gamePlayers = GamePlay::where('game_id', '=', $gameId)->get();
                $gameToFindPoint = Game::find($id);
                $initialGamePoint = $gameToFindPoint->initial_point;
                $finalGamePoint   = $gameToFindPoint->final_point;
                if(count($gamePlayers) > 3 && count($gamePlayers) <= 10) {
                    // minus 15% point from the initial game point
                      $percentage = 15 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                     // Update game point
                     $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                } elseif(count($gamePlayers) > 10 && count($gamePlayers) <= 20 ) {
                      // minus 25% point from the initial game point
                      $percentage = 25 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                } elseif(count($gamePlayers) > 20  && count($gamePlayers) <= 30) {
                       // minus 35% point from the initial game point
                      $percentage = 35 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                } elseif(count($gamePlayers) > 30 && count($gamePlayers) <= 40) {
                     // minus 45% point from the initial game point
                      $percentage = 45 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                } elseif (count($gamePlayers) > 40 && count($gamePlayers) <= 50) {
                      // minus 55% point from the initial game point
                      $percentage = 55 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                } elseif (count($gamePlayers) > 50 && count($gamePlayers) <= 60) {
                      // minus 65% point from the initial game point
                      $percentage = 65 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                }elseif (count($gamePlayers) > 60 && count($gamePlayers) <= 70) {
                      // minus 75% point from the initial game point
                      $percentage = 75 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                }elseif (count($gamePlayers) > 70  && count($gamePlayers) <= 80) {
                      // minus 85% point from the initial game point
                      $percentage = 85 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                     // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                }elseif (count($gamePlayers) > 80 && count($gamePlayers) <= 90) {
                      // minus 95% point from the initial game point
                      $percentage = 95 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                }elseif (count($gamePlayers) > 90 && count($gamePlayers) <= 150) {
                      // minus 98% point from the initial game point
                      $percentage = 98 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                     // Update game point
                     $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                }elseif (count($gamePlayers) > 150) {
                      // minus 98.5% point from the initial game point
                      $percentage = 98.5 / 100 * $initialGamePoint;
                      $newGamePoint = $initialGamePoint - $percentage;
                      // Update game point
                      $query = DB::update("UPDATE games SET final_point='$newGamePoint' WHERE id='$gameId'");
                }
                return redirect()->back()->with('success', 'Game played successfully!');
     }

    }


    public function gameWinners($id) {
        $gamePlay = Game::find($id);
        $correct_word = $gamePlay->correct_word;
        if ($gamePlay->result_status != "Out") {
            return redirect()->back()->with('error', 'Result not yet out');
        } else {
            $page_description = "wordPay | Online gaming platform for making money finding words";
            $page_title     = "wordPay | Winners ";
            $page_keywords = "wordPay, wordpay, online money, trading";
           $position = 1;
           $winners = winners::where('game_id', '=', $id)->get();
            return view('winners')->with('page_description', $page_description)
                                  ->with('page_title', $page_title)
                                  ->with('page_keywords', $page_keywords)
                                  ->with('gamePlay', $gamePlay)
                                  ->with('position', $position)
                                  ->with('winners', $winners);
        }

    }
}
