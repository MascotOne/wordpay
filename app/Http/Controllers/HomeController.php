<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\GamePlay;
use App\Positions;
use App\DisplayMessages;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth()->user()->user_type == "player") {
        $game = Game::where('game_status', '=', 'playing')->get();
        $positions = Positions::all();
        $page_title = "wordPay | Online gaming platform for making money knowing your dictionary";
        $page_description = "wordPay is an online gaming platform where players can use as litle as 100 naira to predict words and make 100 of thousands or even millions after getting the right word";
        $page_keywords = "play games, online games, how to make money online, making money online, sure way of making money in Nigeria, make money in nigeria";
       // $topWinners     = GamePlay::where();
        return view('home')->with('page_title', $page_title)
                            ->with('game', $game)
                            ->with('page_description', $page_description)
                            ->with('positions', $positions)
                            ->with('page_keywords', $page_keywords); 
        } else {
            $games = Game::all();
        return view('admins.home')->with('games', $games);
        }
       
    }

   public function wordGames() {
       if(Auth()->user()->user_type == "player") {
        $page_title = "wordPay | Word Games";
        $page_description = "wordPay is an online gaming platform where players can use as litle as 100 naira to predict words and make 100 of thousands or even millions after getting the right word";
        $page_keywords = "play games, online games, how to make money online, making money online, sure way of making money in Nigeria, make money in nigeria";
        $wordGames = Game::where([['game_status', '=', 'playing'],
                                  ['game_type', '=', 'words']
                                    ])
                                    ->orderBy('id', 'desc')->get();
        return view('word-games')->with('page_title', $page_title)
                                 ->with('page_keywords', $page_keywords)
                                 ->with('page_description', $page_description)
                                 ->with('wordGames', $wordGames);
    }else {
           return redirect()->back();
       }
   }

   public function quoteGames() {
    if(Auth()->user()->user_type == "player") {
     $page_title = "wordPay | Quote Games";
     $page_description = "wordPay is an online gaming platform where players can use as litle as 100 naira to predict words and make 100 of thousands or even millions after getting the right word";
     $page_keywords = "play games, online games, how to make money online, making money online, sure way of making money in Nigeria, make money in nigeria";
     $quoteGames = Game::where([['game_status', '=', 'playing'],
                               ['game_type', '=', 'quotes']
                                 ])
                                 ->orderBy('id', 'desc')->get();
     return view('quote-games')->with('page_title', $page_title)
                              ->with('page_keywords', $page_keywords)
                              ->with('page_description', $page_description)
                              ->with('quoteGames', $quoteGames);
 }else {
        return redirect()->back();
    }
}
}
