<?php

namespace App\Http\Controllers;
use App\Game;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index() {
        $page_title = "wordPay | Online gaming platform for making money knowing your dictionary";
        $page_description = "wordPay is an online gaming platform where players can use as litle as 100 naira to predict words and make 100 of thousands or even millions after getting the right word";
        $page_keywords = "play games, online games, how to make money online, making money online, sure way of making money in Nigeria, make money in nigeria";
        $game = Game::where('game_status', '=', 'playing')->get();
        return view('index')->with('page_title', $page_title)
                            ->with('page_description', $page_description)
                            ->with('game',$game)
                            ->with('page_keywords', $page_keywords);
    }
}
