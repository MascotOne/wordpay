<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticController extends Controller
{

    public function howToPlay() {
        $page_title = "wordPay | How to play";
        $page_description = "wordPay is an online gaming platform for making money";
        $page_keywords      = "wordpay, online games, make money online, making money knowing words, make miney finding words, know quotes and make money";
        return view('how-to-play')->with('page_title', $page_title)
                                  ->with('page_description', $page_description)
                                  ->with('page_keywords', $page_keywords);
    }

    public function help() {
        $page_title = "wordPay | Help";
        $page_description = "wordPay is an online gaming platform for making money";
        $page_keywords      = "wordpay, online games, make money online, making money knowing words, make miney finding words, know quotes and make money";
        return view('help')->with('page_title', $page_title)
                                  ->with('page_description', $page_description)
                                  ->with('page_keywords', $page_keywords);
    }

    public function contactUs() {
        $page_title = "wordPay | Contact us";
        $page_description = "wordPay is an online gaming platform for making money";
        $page_keywords      = "wordpay, online games, make money online, making money knowing words, make miney finding words, know quotes and make money";
        return view('contact-us')->with('page_title', $page_title)
                                  ->with('page_description', $page_description)
                                  ->with('page_keywords', $page_keywords);
    }

   public function termsAndConditions() {
    $page_title = "wordPay | Terms and Conditions";
    $page_description = "wordPay is an online gaming platform for making money";
    $page_keywords      = "wordpay, online games, make money online, making money knowing words, make miney finding words, know quotes and make money";
    return view('terms')->with('page_title', $page_title)
                        ->with('page_description', $page_description)
                        ->with('page_keywords', $page_keywords);
   }
}
