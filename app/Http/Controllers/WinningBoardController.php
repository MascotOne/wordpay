<?php

namespace App\Http\Controllers;
use App\winners;
use App\User;
use Illuminate\Http\Request;

class WinningBoardController extends Controller
{
    public function winners() {
        $page_title = "wordPay | Winners";
        $page_description = "wordPay | Online gaming platform";
        $page_keywords    = "wordpay, online game";
        $winners = winners::all();
        $sn = 1;
        return view('winning-board')->with('winners', $winners)
                                    ->with('page_keywords', $page_keywords)
                                    ->with('page_description', $page_description)
                                    ->with('sn', $sn)
                                    ->with('page_title', $page_title);
}
}
