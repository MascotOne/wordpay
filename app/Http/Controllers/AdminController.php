<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\GamePlay;
use Carbon\Carbon;
use App\User;
use DB;
use App\Positions;
use App\Payout;
use App\Transactions;
use App\DisplayMessages;
class AdminController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin() {
        $page_description = "Online game platform for making money";
        $page_title ="piedGame | Admin";
        $page_keywords = "";
            return view('admins.home')->with('page_description', $page_description)
            ->with('page_title', $page_title)
            ->with('page_keywords', $page_keywords);
    }

    public function addWord() {
        $page_description = "Online game platform for making money";
        $page_title ="wordPay | Add Word";
        $page_keywords = "";
        if(Auth()->user()->user_type != "admin") {
            return redirect()->back();
        } else {
            return view('admins.add-word')->with('page_description', $page_description)
            ->with('page_title', $page_title)
            ->with('page_keywords', $page_keywords);
        }
  }

  public function saveWord(Request $request) {
      if(Auth()->user()->user_type !=="admin") {
          return redirect()->back();
      } else {
          $this->validate($request, [
              'enword' => 'required|string',
              'correct_word' => 'required|string',
              'meaning'      => 'required|string',
              'instruction'  => 'required|string',
              'start_time'   => 'required',
              'end_time'     => 'required',
              'duration'     => 'required',
              'game_point'   => 'required'
          ]);

          $addWord = new Game;
          $max = 6;
          $min = 4;
          $addWord->game_id = "wordPay:". rand($min,$max);
          $addWord->enscripted_word = $request->input('enword');
          $addWord->correct_word    = $request->input('correct_word');
          $addWord->meaning         = $request->input('meaning');
          $addWord->admin_name      = Auth()->user()->fname. " ". Auth()->user()->lname;
          $addWord->admin_id        = Auth()->user()->id;
          $addWord->duration        = $request->input('duration');
          $addWord->instruction     = $request->input('instruction');
          $addWord->start_time      = $request->input('start_time');
          $addWord->end_time        = $request->input('end_time');
          $addWord->game_status     = "Pending";
          $addWord->final_point      = $request->input('game_point');
          $addWord->initial_point   = $request->input('game_point');
          $addWord->game_type       = $request->input('game_type');
          $addWord->save();
          return redirect()->back()->with('success', 'Game added successfully');
      }
  }

   public function startAllGame() {
     if(Auth()->user()->user_type != "admin") {
       return redirect()->back();
     } else {
       $gameToStart = Game::where('game_status', '=', 'Pending')->get();
       $gameToStart->game_status  = "playing";
       $gameToStart->update();
       return redirect()->back()->with('success', 'All pending games have been started');
     }
   }
     public function games() {
         $page_description = "";
         $page_title = "wordPay | Games";
         $page_keywords = "";
       if(Auth()->user()->user_type != "admin") {
           return redirect()->back();
       }  else {

        $games = Game::orderBy('id', 'DESC')->get();
        return view('admins.games')->with('page_description', $page_description)
                                       ->with('page_title', $page_title)
                                       ->with('page_keywords', $page_keywords)
                                       ->with('games', $games);
       }

     }


     public function gameDetails($id) {
      if(Auth()->user()->user_type != "admin") {
          return redirect()->back();
      }  else {
       $game = Game::find($id);
       $page_description = "";
       $page_title = "wordPay |". $game->correct_word;
       $page_keywords = "";
       $players = GamePlay::where('game_id', '=', $id)->get();
       $i = 1;
       $countPlayers = count($players);
       $winners = GamePlay::where('word_played', '=', $game->correct_word)->get();
       $countWinners = count($winners);
       $sn = 1;
       return view('admins.game-details')->with('page_description', $page_description)
                                      ->with('page_title', $page_title)
                                      ->with('page_keywords', $page_keywords)
                                      ->with('players', $players)
                                      ->with('i', $i)
                                      ->with('sn', $sn)
                                      ->with('countPlayers', $countPlayers)
                                      ->with('winners', $winners)
                                      ->with('countWinners', $countWinners)
                                      ->with('game', $game);

      }

    }
  public function players() {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
      $players = User::where('user_type', '=', 'Player')->get();
      $countPlayers = count($players);
      $i = 1;
     return view('admins.players')->with('players', $players)
                                  ->with('countPlayers', $countPlayers)
                                  ->with('i', $i);
    }
  }

  public function makeAdmin($id) {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
         $userToMakeAdmin = User::find($id);
         $userToMakeAdmin->user_type = "admin";
         $userToMakeAdmin->update();
         return redirect()->back()->with('success', 'Player has been appointed an admin');
    }
  }

  public function makePlayer($id) {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
         $userToMakeAdmin = User::find($id);
         $userToMakeAdmin->user_type = "player";
         $userToMakeAdmin->update();
         return redirect()->back()->with('success', 'Admin has been made player');
    }
  }


  public function admins() {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
      $admins = User::where('user_type', '=', 'admin')->get();
      $countAdmins = count($admins);
      $i = 1;
      return view('admins.admins')->with('admins', $admins)
                                  ->with('countAdmins', $countAdmins)
                                  ->with('i', $i);
    }
  }

  public function editPlayBalance(Request $request, $id) {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
      $playerToEditAccount = User::find($id);
      $previousBalance = $playerToEditAccount->account_balance;
      $editedBalance = $request->input('play_balance');
      $playerToEditAccount->account_balance = $previousBalance + $editedBalance;
      $playerToEditAccount->update();

      $userFname = $playerToEditAccount->fname;
      $userLname = $playerToEditAccount->lname;
      $userId = $playerToEditAccount->id;

      $transaction  = new Transactions;
      $transaction->transaction_type = "Credit Account";
      $transaction->trans_amount = $request->input('play_balance');
      $transaction->trans_status = "Successfull";
      $transaction->fname = $userFname;
      $transaction->lname = $userLname;
      $transaction->trans_id = "Credit".rand();
      $transaction->user_id = $userId;
      $transaction->save();
        return redirect()->back()->with('success', 'Account Updated');
    }
  }

  public function deletePlayer($id) {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
         $playerToDelete = User::find($id);
         $playerToDelete->delete();

         // Feature to add later
         // Delete transaction history if exist

         return redirect()->back()->with('success', 'Player has been deleted');
    }
  }


  public function deleteAdmin($id) {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
         $adminToDelete = User::find($id);
         $adminToDelete->delete();
         return redirect()->back()->with('success', 'Admin has been deleted');
    }
  }

  public function payouts() {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    }else {
      $payouts = Payout::all();
      $i = 1;
      return view('admins.payouts')->with('payouts', $payouts)
                                   ->with('i', $i);
    }
  }

  public function updatePayout($id) {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
      $payoutToUpdate = Payout::find($id);
      $payoutToUpdate->status = "Paid";
      $payoutToUpdate->update();
      $payoutAmount = $payoutToUpdate->payout_amount;
      $playerToSaveTrans = $payoutToUpdate->user_id;

      $userToFind  = User::find($playerToSaveTrans);
      $userFname   = $userToFind->fname;
      $userLname   = $userToFind->lname;
      $userId      =  $userToFind->id;


      $transaction  = new Transactions;
      $transaction->transaction_type = "Credit Account";
      $transaction->trans_amount = $payoutAmount;
      $transaction->trans_status = "Successfull";
      $transaction->fname = $userFname;
      $transaction->lname = $userLname;
      $transaction->user_id = $userId;
      $transaction->trans_id = "Payout".rand();
      $transaction->save();
      return redirect()->back()->with('success', 'Player payout has been paid');
    }
  }

  public function pendingPayout() {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
      $i = 1;
      $pendingPayouts = Payout::where('status', '=', 'Pending')->get();
      return view('admins.pending-payouts')->with('i', $i)
                                           ->with('pendingPayouts', $pendingPayouts);
    }
  }

  // Start game
  public function startGame($id) {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    } else {
      $gameToStart  = Game::find($id);
      $gameToStart->game_status = "playing";
      $gameToStart->update();
      return redirect()->back()->with('success', 'Game started Successfullt!!!!');
    }
  }


// End game
  public function endGame($id) {

      $gameToEnd = Game::find($id);
      $gameToEnd->game_status = "Ended";
      $gameToEnd->update();
      $correct_word = $gameToEnd->correct_word;

      // Update the winners account with their winnings amount
      // First find the winners by id from the game play
      $gamePlay = GamePlay::where([
                                ['game_id', '=', $id],
                                ['word_played', '=', $correct_word]
                                ])->get();
      foreach ($gamePlay as $game) {
          $amountStaked  = $game->played_amount;
          $gamePoint     = $game->game_point;
          $winning       = $amountStaked * $gamePoint;
          $playerId      = $game->player_id;
          $game_status = "Ended";
         
          $UpdateGameStatus = DB::update("UPDATE  game_plays SET game_status='$game_status' WHERE game_id=$id");
        
         
            // Get player previous balance
         $playerToFind = User::findOrFail($playerId);
        
         $previousBalance = $playerToFind->account_balance;
         $newBalance = $previousBalance + $winning;
         $update = DB::update("UPDATE users SET account_balance = $newBalance WHERE id=$playerId");
  
      return redirect()->back()->with('success', 'Game ended!!!');
    }
  }
  
  public function startAllGames() {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    }else {
      $startAll = DB::update("UPDATE games SET game_status='playing' WHERE game_status='Pending'");
      return redirect()->back()->with('success', 'All the games has been started');
    }
  }

  public function endAllGames() {
    if(Auth()->user()->user_type != "admin") {
      return redirect()->back();
    }else {
      $query = DB::update("UPDATE games SET game_status='Ended' WHERE game_status='playing'");


      // set game_status of each played word to ended
      $game_status = DB::update("UPDATE game_plays SET game_status='Ended' WHERE game_status='playing'");
      return redirect()->back()->with('success', 'All games has been ended');
    }
  }

  public function findUsers(Request $request) {
     $this->validate($request, [
      'find_user' => 'required'
    ]);
   $i = 1;
    $search = $request->input('find_user');
    $userToFind = User::where('fname','=', $search)
                          ->orwhere('lname', '=', $search)
                          ->orWhere('email', '=', $search)->get();
                          return view('admins.search')->with('search', $search)
                          ->with('i', $i)->with('userToFind', $userToFind);
  }

}
