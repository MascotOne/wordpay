<?php

namespace App\Http\Controllers;
use App\User;
use App\GamePlay;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

public function playHistory($id) {
    $page_title = "wordPay | History";
    $page_description = "An online gaming and gambling platform";
    $page_keywords    = "wordPay, wordpay, online games, gambling";
    $playerId =      Auth()->user()->id;
    $history = GamePlay::where('player_id', '=', $playerId)->orderBy('id', 'DESC')->get();
    return view('play-history')->with('page_title', $page_title)
                                     ->with('page_description', $page_description)
                                     ->with('page_keywords', $page_keywords)
                                     ->with('history', $history);
}

public function historyDetails($id) {
    $page_title = "wordPay | History";
    $page_description = "An online gaming and gambling platform";
    $page_keywords    = "wordPay, wordpay, online games, gambling";

    $historyDetails = GamePlay::find($id);
    return view('history-details')->with('page_description', $page_description)
                                   ->with('page_title', $page_title)
                                   ->with('page_keywords', $page_keywords)
                                   ->with('historyDetails', $historyDetails);
}
}
