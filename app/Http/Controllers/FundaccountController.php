<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FundaccountController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     *
     * public function __construct()
     * {
     *    $this->middleware('auth');
     * }
     */

    public function index() {
        $page_title = "Fund your account now | wordPay";
        $page_description = "wordPay is an online gaming platform where players can use as litle as 100 naira to predict words and make 100 of thousands or even millions after getting the right word";
        $page_keywords = "play games, online games, how to make money online, making money online, sure way of making money in Nigeria, make money in nigeria";
        return view('fund-account')->with('page_title', $page_title)
                            ->with('page_description', $page_description)
                            ->with('page_keywords', $page_keywords);
    }
}
