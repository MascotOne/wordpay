<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('game_id');
            $table->string('enscripted_word');
            $table->string('correct_word');
            $table->mediumText('meaning');
            $table->string('admin_name');
            $table->integer('admin_id');
            $table->string('duration');
            $table->mediumText('instruction');
            $table->time('start_time');
            $table->time('end_time');
            $table->string('game_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
