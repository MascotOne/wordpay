<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamePlaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_plays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('word_played');
            $table->string('correct_word');
            $table->string('enscripted_word');
            $table->string('player_fname');
            $table->string('player_lname');
            $table->integer('player_id');
            $table->string('player_number');
            $table->string('game_point');
            $table->string('played_amount');
            $table->integer('game_id');
            $table->string('marked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_plays');
    }
}
